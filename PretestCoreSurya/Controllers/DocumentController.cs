﻿using Dapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PretestCoreSurya.Model;
using PretestCoreSurya.Repository.Interface;
using System.Data;

namespace PretestCoreSurya.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentController : Controller
    {
        private readonly IDocument _authentication;
        public DocumentController(IDocument authentication)
        {
            _authentication = authentication;
        }

        [HttpGet("DocumentList")]
        [Authorize(Roles = "Admin")]
        public IActionResult getDocument()
        {
            var result = _authentication.getDocumentList<ModelDocument>();

            return Ok(result);
        }
        [HttpPost("Create")]
        [Authorize(Roles = "Admin")]
        public IActionResult CreateDocument([System.Web.Http.FromBody] ModelDocument document)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("idcompany", document.IDCompany, DbType.String);
            dp_param.Add("idcategory", document.IDCategory, DbType.String);
            dp_param.Add("name", document.Name, DbType.String);
            dp_param.Add("description", document.Description, DbType.String);
            dp_param.Add("flag", document.Flag, DbType.String);
            dp_param.Add("createdby", document.CreatedBy, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelDocument>("sp_CreateDocument", dp_param);
            if (result.Code == 200)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }

        [HttpPut("Update")]
        [Authorize(Roles = "Admin")]
        public IActionResult UpdateCategory([System.Web.Http.FromBody] ModelDocument document, string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("idcompany", document.IDCompany, DbType.String);
            dp_param.Add("idcategory", document.IDCategory, DbType.String);
            dp_param.Add("name", document.Name, DbType.String);
            dp_param.Add("description", document.Description, DbType.String);
            dp_param.Add("flag", document.Flag, DbType.String);
            dp_param.Add("createdby", document.CreatedBy, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelDocument>("sp_UpdateDocument", dp_param);
            if (result.Code == 200)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }

        [HttpDelete("Delete")]
        [Authorize(Roles = "Admin")]
        public IActionResult Delete(string id)
        {
            if (id == string.Empty)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelDocument>("sp_DeleteDocument", dp_param);

            if (result.Code == 200)
            {
                return Ok(result);
            }

            return NotFound(result);
        }

    }
}
