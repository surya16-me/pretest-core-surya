﻿using Dapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PretestCoreSurya.Model;
using PretestCoreSurya.Repository.Interface;
using PretestCoreSurya.Repository.Services;
using System.Data;

namespace PretestCoreSurya.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PositionController : Controller
    {
        private readonly IPosition _authentication;
        public PositionController(IPosition authentication)
        {
            _authentication = authentication;
        }

        [HttpGet("PositionList")]
        [Authorize(Roles = "Admin")]
        public IActionResult getPosition()
        {
            var result = _authentication.getPositionList<ModelPosition>();

            return Ok(result);
        }
        [HttpPost("Create")]
        [Authorize(Roles = "Admin")]
        public IActionResult CreatePosition([System.Web.Http.FromBody] ModelPosition position)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("name", position.Name, DbType.String);
            dp_param.Add("CreatedBy", position.CreatedBy, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelPosition>("sp_CreatePosition", dp_param);
            if (result.Code == 200)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }

        [HttpPut("Update")]
        [Authorize(Roles = "Admin")]
        public IActionResult UpdatePosition([System.Web.Http.FromBody] ModelPosition position, string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("name", position.Name, DbType.String);
            dp_param.Add("CreatedBy", position.CreatedBy, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelPosition>("sp_UpdatePosition", dp_param);
            if (result.Code == 200)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }

        [HttpDelete("Delete")]
        [Authorize(Roles = "Admin")]
        public IActionResult DeletePosition(string id)
        {
            if (id == string.Empty)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelPosition>("sp_DeletePosition", dp_param);

            if (result.Code == 200)
            {
                return Ok(result);
            }

            return NotFound(result);
        }

    }
}
