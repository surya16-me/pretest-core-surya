﻿using System.ComponentModel.DataAnnotations;

namespace PretestCoreSurya.Model
{
    public class ModelCompany
    {
        [Required]
        public string? Name { get; set; }

        [Required]
        public string? Address { get; set; }

        [Required]
        public string? Email { get; set; }

        [Required]
        public string? Telephone { get; set; }

        [Required]
        public int? Flag { get; set; }

        [Required]
        public int? CreatedBy { get; set; }


        public DateTime Date { get; set; } = DateTime.Now;
    }
}
