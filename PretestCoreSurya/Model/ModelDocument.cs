﻿using System.ComponentModel.DataAnnotations;

namespace PretestCoreSurya.Model
{
    public class ModelDocument
    {
        [Required]
        public int IDCompany { get; set; }

        [Required]
        public int IDCategory { get; set; }

        [Required]
        public string? Name { get; set; }

        [Required]
        public string? Description { get; set; }

        [Required]
        public int? Flag { get; set; }

        [Required]
        public int? CreatedBy { get; set; }


        public DateTime Date { get; set; } = DateTime.Now;
    }
}
