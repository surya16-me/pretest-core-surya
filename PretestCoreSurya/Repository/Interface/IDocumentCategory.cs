﻿using Dapper;
using PretestCoreSurya.Model;

namespace PretestCoreSurya.Repository.Interface
{
    public interface IDocumentCategory
    {
        Response<T> Execute_Command<T>(string query, DynamicParameters sp_params);
        Response<List<T>> getDocumentCategoryList<T>();
    }
}
