﻿using Dapper;
using PretestCoreSurya.Model;

namespace PretestCoreSurya.Repository.Interface
{
    public interface IPosition
    {
        Response<T> Execute_Command<T>(string query, DynamicParameters sp_params);
        Response<List<T>> getPositionList<T>();
    }
}
